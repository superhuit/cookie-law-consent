# [2.0.2] - 2022-03-17

## Added

- Google Maps service
- Expose configs to the settings REST API
