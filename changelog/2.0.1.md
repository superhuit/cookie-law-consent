# [2.0.1] - 2022-01-11

## Fixed
- Get GDPR configs even if no services are configured (we can still need it for necessary cookies)

